.DEFAULT_GOAL := up
.PHONY:	up down requirements depends-tools clean

# run entire compass application
up:
	docker compose -p gorm-snippet -f docker/docker-compose.yml up -d

# stop and clean up entire compass application
down:
	docker compose -p gorm-snippet down

# meta target
requirements: depends-tools

# install tools which is needed.
depends-tools:
	go install github.com/githubnemo/CompileDaemon@latest

# clean up
clean:
	rm -rf dev-mysql-db-data
