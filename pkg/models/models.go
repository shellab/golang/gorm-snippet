package models

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Application struct {
	gorm.Model
	Name      string
	AppID     uuid.UUID `gorm:"unique"`
	Secret    string
	Tenants   []Tenant
	Subjects  []Subject
	CreatedAt int // unix timestamp
	UpdatedAt int
}

type Tenant struct {
	gorm.Model
	ApplicationID   uint
	Name            string
	Subjects        []Subject `gorm:"many2many:tenant_subjects"`
	TenantResources []TenantResource
	TenantRoles     []TenantRole
	CreatedAt       int
	UpdatedAt       int
}

type Subject struct {
	gorm.Model
	ApplicationID uint
	ExternalID    string
	Name          string
	Tenants       []Tenant `gorm:"many2many:tenant_subjects"`
	CreatedAt     int
	UpdatedAt     int
}

type TenantSubject struct { // ref
	gorm.Model
	TenantID    uint
	Tenant      Tenant // belongs to Tenant
	SubjectID   uint
	Subject     Subject      // belongs to Subject
	TenantRoles []TenantRole `gorm:"many2many:tenant_role_subjects"`
	CreatedAt   int
	UpdatedAt   int
}

type TenantRole struct {
	gorm.Model
	Name                  string
	TenantID              uint
	TenantSubjects        []TenantSubject        `gorm:"many2many:tenant_role_subjects"`
	TenantResourceActions []TenantResourceAction `gorm:"many2many:tenant_role_actions"`
	CreatedAt             int
	UpdatedAt             int
}

type TenantRoleSubject struct { // ref
	gorm.Model
	TenantSubjectID uint
	TenantRoleID    uint
	CreatedAt       int
	UpdatedAt       int
}

type TenantResource struct {
	gorm.Model
	Name                  string
	TenantID              uint
	TenantResourceActions []TenantResourceAction
	CreatedAt             int
	UpdatedAt             int
}

type TenantResourceAction struct {
	gorm.Model
	TenantResourceID uint
	Name             string
	TenantResource   TenantResource
	TenantRoles      []TenantRole `gorm:"many2many:tenant_role_actions"`
	CreatedAt        int
	UpdatedAt        int
}

type TenantRoleAction struct { // ref
	gorm.Model
	TenantResourceActionID uint
	TenantRoleID           uint
	CreatedAt              int
	UpdatedAt              int
}
