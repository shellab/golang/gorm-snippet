package database

import (
	"fmt"
	"gorm-snippet/pkg/models"
	"os"
	"sync"

	log "github.com/sirupsen/logrus"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var once sync.Once
var singletonDB *gorm.DB

func GetDB() *gorm.DB {
	once.Do(func() {
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWD"),
			os.Getenv("DB_HOST"),
			3306,
			os.Getenv("DB_NAME"),
		)
		db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
			DisableForeignKeyConstraintWhenMigrating: true,
		})
		if err != nil {
			log.Fatalln("Can't connect to database with DSN:", dsn)
		}
		singletonDB = db.Debug()
	})
	return singletonDB
}

func GormAutoMigrate(db *gorm.DB) {
	if err := db.AutoMigrate(
		&models.Application{},
		&models.Tenant{},
		&models.Subject{},
		&models.TenantSubject{},
		&models.TenantRole{},
		&models.TenantRoleSubject{},
		&models.TenantResource{},
		&models.TenantResourceAction{},
		&models.TenantRoleAction{},
	); err != nil {
		log.Fatalln("database migrate failed with error:", err)
	}
}
