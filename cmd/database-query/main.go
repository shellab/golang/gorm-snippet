package main

import (
	"errors"
	"fmt"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()

	// init data
	db.Create(&models.Application{
		Name: "test application",
	})


	// fetch first, take, last will add limit 1
	// first, last will add order by rule
	app := models.Application{}
	r := db.First(&app)
	if r.Error != nil {
		if errors.Is(r.Error, gorm.ErrRecordNotFound) { // only for first, last, take
			fmt.Println("record not found")
		} else {
			fmt.Println(r.Error)
		}
	}

	// retriev by PK
	getApp := models.Application{}
	getApp.ID = 1
	r = db.Take(&getApp)
	if r.Error != nil {
		fmt.Println(r.Error)
	}

	// find data
	// without limit condition
	apps := []models.Application{}
	db.Find(&apps) // 如果給 slice 就是把所有找到結果放進去，如果給 struct 只放第一筆進去
	for _, a := range apps {
		fmt.Println(a.Name)
	}


	// find data
	// where
	// select
	// order
	// limit
	// offset
	condictionApps := []models.Application{}
	r = db.Select("name").
		Where("name like ?", "%test%").
		Order("id desc").
		Limit(10).
		Offset(0).
		Find(&condictionApps)
	// SELECT `name` FROM `applications` WHERE name like '%test%' AND `applications`.`deleted_at` IS NULL ORDER BY id desc LIMIT 10
	if r.Error != nil {
		fmt.Println(r.Error)
	}
	fmt.Println(condictionApps[0].Name)


	// group by having
	// 因為回來的資料已經沒有 model 描述，因此需要定義可以接受回傳結果的 struct
	// 因為沒有 model 可以描述 table name 因此透過 Model() 強制指定目標的 table
	type groupResult struct {
		Name string
		Counter uint
	}
	queryResult := groupResult{}
	r = db.Model(&models.Application{}).Select("name, count(1) as counter").
		Group("name").Having("name like ?", "%test%").
		Find(&queryResult)
	if r.Error != nil {
		fmt.Println(r.Error)
	}
	// SELECT name, count(1) as counter FROM `applications` WHERE `applications`.`deleted_at` IS NULL GROUP BY `name` HAVING name like '%test%'
	fmt.Println(queryResult.Name, queryResult.Counter)


	// find data
	// distinct
	distApps := []models.Application{}
	r = db.Distinct("name").Order("name desc").Find(&distApps)
	if r.Error != nil {
		fmt.Println(r.Error)
	}
	// SELECT DISTINCT `name` FROM `applications` WHERE `applications`.`deleted_at` IS NULL ORDER BY name desc
	fmt.Println(condictionApps[0].Name)


	// count data
	// count 沒有 model 結構，因此要補個 Model 讓 orm 知道是那個 table
	var counter int64
	r = db.Model(&models.Application{}).Count(&counter)
	if r.Error != nil {
		fmt.Println(r.Error)
	}
	fmt.Println("applications:", counter)


}
