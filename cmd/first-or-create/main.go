package main

import (
	"fmt"

	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
	"github.com/sanity-io/litter"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()

	// first or create + assign
	// 只能用在 Where 是 map 或是 struct 建議使用 map 因為 struct 的 zero-value 不會被考慮
	app := models.Application{}
	r := db.Where(map[string]interface{}{ // 用 map
		"name": "compass",
	}).Assign(models.Application{
		AppID: uuid.New(),
	}).FirstOrCreate(&app)
	if r.Error != nil {
		fmt.Println(r.Error)
	}
	litter.Dump(app)
	// SELECT * FROM `applications` WHERE `name` = 'compass' AND `applications`.`deleted_at` IS NULL ORDER BY `applications`.`id` LIMIT 1
	// UPDATE `applications` SET `app_id`='5a395fd7-95eb-496e-afe4-7b57c27ee1aa',`updated_at`='2023-03-22 11:51:58.734' WHERE `name` = 'compass' AND `applications`.`deleted_at` IS NULL AND `id` = 1
}
