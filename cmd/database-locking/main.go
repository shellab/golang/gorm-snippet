package main

import (
	"fmt"
	"time"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()
	// locking
	// select for update
	// 在當一個 transaction 中，做 select + update 時，外面任何的 select or update 對該 row 操作都會等待
	// 直到 transaction 完成
	// 若 select for update 沒加上 transaction 的話，就無效，視為單一操作 select

	// transacation pattern
	// if err := p.db.Transaction(func(tx *gorm.DB) error {
	// 	return nil
	// }); err != nil {
	// 	return err
	// }
	if err := db.Transaction(func(tx *gorm.DB) error {
		app := models.Application{}
		app.ID = 1

		r := tx.Clauses(clause.Locking{Strength: "UPDATE"}).Take(&app)
		if r.Error != nil {
			return r.Error
		}
		fmt.Println("++++++++ select for update and wait... +++++++")
		// 任何對該 row 的操作都會等待，直接到這個 transaction 完成

		time.Sleep(time.Duration(10) * time.Second)

		fmt.Println("++++++++ start to update +++++++")
		app.Name = "test helloworld"
		r = tx.Save(&app)
		if r.Error != nil {
			return r.Error
		}

		return nil
	}); err != nil {
		fmt.Println("transaction failed with", err)
	}
}
