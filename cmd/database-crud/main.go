package main

import (
	"fmt"

	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()
	//database.GormAutoMigrate(db)

	/*
		create upsert
	*/
	app := models.Application{
		AppID: uuid.New(),
		Name:  "storage service",
		Tenants: []models.Tenant{
			{
				Name: "pod",
				TenantResources: []models.TenantResource{
					{
						Name: "/api/v1/health",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
						},
					},
					{
						Name: "/api/v1/item",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
							{
								Name: "write",
							},
						},
					},
				},
			},
			{
				Name: "billing",
			},
		},
	}
	db.Create(&app)

	/*
		append new tenant and resource
	*/
	app.Tenants = append(app.Tenants, models.Tenant{
		Name: "web",
		TenantResources: []models.TenantResource{
			{
				Name: "/api/v1/storage",
				TenantResourceActions: []models.TenantResourceAction{
					{
						Name: "read",
					},
					{
						Name: "write",
					},
					{
						Name: "list",
					},
				},
			},
		},
	})
	db.Save(&app)

	/*
		update data
	*/
	r := db.Model(&models.Application{}).Where("name = ?", "storage service").Updates(map[string]interface{}{
		"name": "storage service v1",
	})
	// UPDATE `applications` SET `name`='storage service v1',`updated_at`='2023-03-22 12:37:36.408' WHERE name = 'storage service' AND `applications`.`deleted_at` IS NULL
	fmt.Println("udpate rows:", r.RowsAffected)

	/*
		or update by PK
		建議不要重用 model 因為其中若有 relation 的話會被 upsert
	*/
	r = db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Updates(map[string]interface{}{
		"name": "storage service v1",
	})
	// UPDATE `applications` SET `name`='storage service v1',`updated_at`='2023-03-22 12:37:36.44' WHERE `applications`.`deleted_at` IS NULL AND `id` = 6
	fmt.Println("udpate rows:", r.RowsAffected)

	/*
		update relation data
	*/
	for _, t := range app.Tenants {
		if t.Name == "pod" {
			for _, r := range t.TenantResources {
				if r.Name == "/api/v1/health" {
					r.Name = "/api/v1/healthz"
					db.Save(&r)
				}
			}
		}
	}

	/*
		query by PK + Preload
	*/
	getApp := models.Application{}
	getApp.ID = app.ID
	r = db.Preload("Tenants.TenantResources.TenantResourceActions").Take(&getApp)
	if r.Error != nil {
		fmt.Println(r.Error)
		return
	}

	for _, t := range getApp.Tenants {
		for _, r := range t.TenantResources {
			for _, a := range r.TenantResourceActions {
				fmt.Println(getApp.Name, t.Name, r.Name, a.Name)
			}
		}
	}

	/*
		delete by PK and its relations
	*/

	deleteApp := models.Application{}
	deleteApp.ID = app.ID
	r = db.Preload("Tenants.TenantResources.TenantResourceActions").Take(&deleteApp)
	if r.Error != nil {
		fmt.Println(r.Error)
		return
	}

	for _, t := range deleteApp.Tenants {
		for _, r := range t.TenantResources {
			db.Select(clause.Associations).Delete(&r) // all relation
		}
		db.Select("TenantResources").Delete(&t)
		// select delete 只能用在 many2many or hasOne or hasMany (因為 relantion 記錄了 PK)
		// 所以 .Delete(&t) 必須帶有 PK 而使用 Where("name = ?", name) 不行因為不是 PK
	}
	db.Select("Tenants").Delete(&deleteApp)
}
