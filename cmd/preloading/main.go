package main

import (
	"fmt"

	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm/clause"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()
	database.GormAutoMigrate(db)

	// init data
	app := models.Application{
		AppID: uuid.New(),
		Name:  "storage service",
		Tenants: []models.Tenant{
			{
				Name: "pod",
				TenantResources: []models.TenantResource{
					{
						Name: "/api/v1/health",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
						},
					},
					{
						Name: "/api/v1/item",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
							{
								Name: "write",
							},
						},
					},
				},
			},
			{
				Name: "billing",
			},
		},
	}
	db.Create(&app)

	/*
		preload 將指定關聯展開
		clause.Associations 只會展開第一層，下面子層的關聯要自已指定
	*/
	getApp := models.Application{}
	getApp.ID = app.ID
	r := db.Preload(clause.Associations).Preload("Tenants.TenantResources.TenantResourceActions").Find(&getApp)
	if r.Error != nil {
		fmt.Println(r.Error)
		return
	}
	// SELECT * FROM `subjects` WHERE `subjects`.`application_id` = 9 AND `subjects`.`deleted_at` IS NULL
	// SELECT * FROM `tenant_resource_actions` WHERE `tenant_resource_actions`.`tenant_resource_id` IN (17,18) AND `tenant_resource_actions`.`deleted_at` IS NULL
	// SELECT * FROM `tenant_resources` WHERE `tenant_resources`.`tenant_id` IN (33,34) AND `tenant_resources`.`deleted_at` IS NULL
	// SELECT * FROM `tenants` WHERE `tenants`.`application_id` = 9 AND `tenants`.`deleted_at` IS NULL
	// SELECT * FROM `applications` WHERE `applications`.`deleted_at` IS NULL AND `applications`.`id` = 9

	/*
		preload with conditions
	*/
	haveHealthApp := models.Application{}
	haveHealthApp.ID = app.ID
	r = db.Preload("Tenants.TenantResources", "name like ?", "%health%").Preload("Tenants.TenantResources.TenantResourceActions").Find(&haveHealthApp)
	if r.Error != nil {
		fmt.Println(r.Error)
		return
	}
	for _, t := range haveHealthApp.Tenants {
		for _, r := range t.TenantResources {
			for _, a := range r.TenantResourceActions {
				fmt.Println(haveHealthApp.Name, t.Name, r.Name, a.Name)
			}
		}
	}
}
