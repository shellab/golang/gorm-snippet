package main

import (
	"fmt"

	"github.com/google/uuid"
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()

	// init data
	app := models.Application{
		AppID: uuid.New(),
		Name:  "storage service",
		Tenants: []models.Tenant{
			{
				Name: "pod",
				TenantResources: []models.TenantResource{
					{
						Name: "/api/v1/health",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
						},
					},
					{
						Name: "/api/v1/item",
						TenantResourceActions: []models.TenantResourceAction{
							{
								Name: "read",
							},
							{
								Name: "write",
							},
						},
					},
				},
			},
			{
				Name: "billing",
			},
		},
	}
	db.Create(&app)

	/*
		associate query
	*/
	// where 條件是 association table 的條件
	// Model(&models.xxxx{PK}) 一定要給 PK 其它不要給
	tenants := []models.Tenant{}
	if err := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Find(&tenants); err != nil {
		fmt.Println(err)
	}
	for _, t := range tenants {
		resources := []models.TenantResource{}
		if err := db.Model(&models.Tenant{Model: gorm.Model{ID: t.ID}}).Where("name like ?", "%item%").Association("TenantResources").Find(&resources); err != nil {
			fmt.Println(err)
		}
		for _, r := range resources {
			actions := []models.TenantResourceAction{}
			if err := db.Model(&models.TenantResource{Model: gorm.Model{ID: r.ID}}).Association("TenantResourceActions").Find(&actions); err != nil {
				fmt.Println(err)
			}
			for _, a := range actions {
				fmt.Println(t.Name, r.Name, a.Name)
			}
		}
	}

	/*
		append association
	*/
	if err := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Append(&models.Tenant{ // slice or struct
		Name: "new tenant",
	}); err != nil {
		fmt.Println(err)
	}
	// INSERT INTO `tenants` (`created_at`,`updated_at`,`deleted_at`,`application_id`,`name`) VALUES ('2023-03-22 13:33:25.139','2023-03-22 13:33:25.139',NULL,1,'new tenant') ON DUPLICATE KEY UPDATE `application_id`=VALUES(`application_id`)
	// UPDATE `applications` SET `updated_at`='2023-03-22 13:33:25.133' WHERE `applications`.`deleted_at` IS NULL AND `id` = 1

	/*
		association count
	*/
	howManyTenants := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Count()
	fmt.Println("there are tenants:", howManyTenants)
	// SELECT count(*) FROM `tenants` WHERE `tenants`.`application_id` = 2 AND `tenants`.`deleted_at` IS NULL

	/*
		replace association
	*/
	newTenant := models.Tenant{
		Name: "the new one",
	}
	db.Create(&newTenant)
	if err := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Replace(&newTenant); err != nil {
		fmt.Println(err)
	}
	// INSERT INTO `tenants` (`created_at`,`updated_at`,`deleted_at`,`application_id`,`name`,`id`) VALUES ('2023-03-22 13:33:25.185','2023-03-22 13:33:25.185',NULL,1,'the new one',4) ON DUPLICATE KEY UPDATE `application_id`=VALUES(`application_id`) <- 建立關聯
	// UPDATE `applications` SET `updated_at`='2023-03-22 13:33:25.22' WHERE `applications`.`deleted_at` IS NULL AND `id` = 1 <- 更新 updated_at
	// UPDATE `tenants` SET `application_id`=NULL WHERE `tenants`.`id` <> 4 AND `tenants`.`application_id` = 1 AND `tenants`.`deleted_at` IS NULL <- 拔掉所有 id 不是 4 的 tenant 關聯

	/*
		delete association

		association 重點在處理關聯，當 delete 時是指把關聯 update 為 NULL
		若要砍資料，用 db.Select(RELATION).Delete(&models{PK})，這個會做 soft delete 當 belongs to, has One, has Many 時會砍到資料
		但是在 Many to Many 情況下作用到 relation table 因此關聯資不會被砍掉
	*/
	if err := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Delete(&newTenant); err != nil {
		fmt.Println(err)
	}
	// UPDATE `tenants` SET `application_id`=NULL WHERE `tenants`.`application_id` = 2 AND `tenants`.`id` = 8 AND `tenants`.`deleted_at` IS NULL

	/*
		clear association
	*/
	if err := db.Model(&models.Application{Model: gorm.Model{ID: app.ID}}).Association("Tenants").Clear(); err != nil {
		fmt.Println(err)
	}
	// UPDATE `tenants` SET `application_id`=NULL WHERE `tenants`.`application_id` = 2 AND `tenants`.`deleted_at` IS NULL
}
