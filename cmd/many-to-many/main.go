package main

import (
	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()
	database.GormAutoMigrate(db)

	// init data
	t := models.Tenant{
		Name: "A team",
		Subjects: []models.Subject{
			{
				Name: "Mars",
			},
			{
				Name: "Kener",
			},
		},
		TenantResources: []models.TenantResource{
			{
				Name: "/api/v1/health",
				TenantResourceActions: []models.TenantResourceAction{
					{
						Name: "read",
					},
				},
			},
			{
				Name: "/api/v1/item",
				TenantResourceActions: []models.TenantResourceAction{
					{
						Name: "read",
					},
					{
						Name: "write",
					},
				},
			},
		},
	}
	db.Create(&t)

	db.Preload(clause.Associations).Take(&models.Tenant{
		Model: gorm.Model{ID: 1},
	})
	/*
		preload 在 many2many 的關係不處理 soft delete
		SELECT * FROM `tenant_subjects` WHERE `tenant_subjects`.`tenant_id` = 1 <- 沒有 deleted_at is null
		SELECT * FROM `subjects` WHERE `subjects`.`id` IN (1,2) AND `subjects`.`deleted_at` IS NULL
		SELECT * FROM `tenant_resources` WHERE `tenant_resources`.`tenant_id` = 1 AND `tenant_resources`.`deleted_at` IS NULL
		SELECT * FROM `tenant_roles` WHERE `tenant_roles`.`tenant_id` = 1 AND `tenant_roles`.`deleted_at` IS NULL
		SELECT * FROM `tenants` WHERE `tenants`.`deleted_at` IS NULL AND `tenants`.`id` = 1 LIMIT 1
	*/

	db.Select("Subjects", "TenantResources", "TenantRoles").Delete(&models.Tenant{
		Model: gorm.Model{ID: 1},
	})
	/*
		select delete 在 many2many 關係不處理 soft delete 因為它是關係，而不是資料
		DELETE FROM `tenant_subjects` WHERE `tenant_subjects`.`tenant_id` = 1 <- 直接 delete 而不是 update
		UPDATE `tenant_resources` SET `deleted_at`='2023-04-07 11:21:26.325' WHERE `tenant_resources`.`tenant_id` = 1 AND `tenant_resources`.`deleted_at` IS NULL
		UPDATE `tenant_roles` SET `deleted_at`='2023-04-07 11:21:26.333' WHERE `tenant_roles`.`tenant_id` = 1 AND `tenant_roles`.`deleted_at` IS NULL
		UPDATE `tenants` SET `deleted_at`='2023-04-07 11:21:26.34' WHERE `tenants`.`id` = 1 AND `tenants`.`deleted_at` IS NULL
	*/
}
