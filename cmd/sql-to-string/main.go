package main

import (
	"fmt"

	_ "github.com/joho/godotenv/autoload"
	"gorm.io/gorm"

	"gorm-snippet/pkg/database"
	"gorm-snippet/pkg/models"
)

func main() {
	db := database.GetDB()
	sqlString := db.ToSQL(func(tx *gorm.DB) *gorm.DB {
		condictionApps := []models.Application{}
		return tx.Select("name").
			Where("name like ?", "%test%").
			Order("id desc").
			Limit(10).
			Offset(0).
			Find(&condictionApps)
	})

	fmt.Println(sqlString)
}
